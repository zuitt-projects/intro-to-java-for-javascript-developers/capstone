package com.tonino;

import java.util.Scanner;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        Scanner myVar = new Scanner(System.in);
        HashMap<String, String> movieList = new HashMap<String, String>();

        System.out.println("\n** B124 Capstone - Movie Store **");
        System.out.println("[Category: Action, Comedy, Drama, Horror, Mystery, Thriller, Romance]\n");

        System.out.print("Would you like to add a movie? (Press Y or N): ");
        var condition = myVar.nextLine();

        while(condition.equals("Y") || condition.equals("y")) {
            System.out.print("\nEnter Movie Name: ");
            var name = myVar.nextLine();

            System.out.print("Enter Category Name: ");
            var category = myVar.nextLine();

            MovieStore movie = new MovieStore(name, category);
            movieList.put(movie.getName(), movie.getCategory());


            System.out.print("Would you like to add again? (Press Y or N): ");
            condition = myVar.nextLine();

            if(condition.equals("Y") || condition.equals("y")) {
                continue;
            } else {
                System.out.println("\n** All Movies **");
                System.out.println(movieList.keySet());
                System.out.println("\nThank you for using this App!");
                break;
            }
        }
    }
}

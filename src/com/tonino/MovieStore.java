package com.tonino;


public class MovieStore extends Movie {

    public MovieStore(String newMovieName, String newCategory) {
        super(newMovieName, newCategory);
    }

    public String getName() {
        return super.getName();
    }

    public String getCategory() {
        return super.getCategory();
    }
}

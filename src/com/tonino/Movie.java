package com.tonino;

public class Movie {
    private String movieName;
    private String category;

    public Movie(String newMovieName, String newCategory) {
        this.movieName = newMovieName;
        this.category = newCategory;
    }

    public String getName() {
        return this.movieName;
    }

    public String getCategory() {
        return this.category;
    }
}
